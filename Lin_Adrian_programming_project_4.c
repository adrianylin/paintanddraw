/* Adrian Lin */

#ifdef __APPLE__
  #include <OpenGL/gl.h>
  #include <OpenGL/glu.h>
  #include <GLUT/glut.h>
#elif __linux__
  #include <GL/glut.h>
#endif

#include <math.h>
#include <stdio.h>

const int WINDOW_WIDTH = 500;
const int WINDOW_HEIGHT = 500;

int state_draw_filled_rectangle = 0;
int state_draw_outlined_rectangle = 0;
int state_draw_filled_ellipse = 0;
int state_draw_outlined_ellipse = 0;
int state_draw_line = 0;
int state_draw_beziercurve = 0;

int x_1 = 0;
int x_2 = 0;
int x_3 = 0;
int x_4 = 0;
int y_1 = 0;
int y_2 = 0;
int y_3 = 0;
int y_4 = 0;

int num_clicks = 0;

/**
 *
 */
void reset_mouse_clicks()
{
    x_1 = 0;
    x_2 = 0;
    x_3 = 0;
    x_4 = 0;
    y_1 = 0;
    y_2 = 0;
    y_3 = 0;
    y_4 = 0;

    num_clicks  = 0;
}

/*
 * Initializes the display. A blank canvas with a black background.
 */
void init_display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glFlush();
}

/*
 * GLUT callback for glutDisplayFunc.
 */
void display()
{
    glFlush();
}

/*
 * Draws a line from two (x, y) coordinates
 */
void draw_line(int x_1, int y_1, int x_2, int y_2)
{
    float x_1_c = ((float) x_1 - glutGet(GLUT_WINDOW_WIDTH) / 2.0) / (glutGet(GLUT_WINDOW_WIDTH) / 2.0);
    float y_1_c = (-1) * ((float) y_1 - glutGet(GLUT_WINDOW_HEIGHT) / 2.0) / (glutGet(GLUT_WINDOW_HEIGHT) / 2.0);
    float x_2_c = ((float) x_2 - glutGet(GLUT_WINDOW_WIDTH) / 2.0) / (glutGet(GLUT_WINDOW_WIDTH) / 2.0);
    float y_2_c = (-1) * ((float) y_2 - glutGet(GLUT_WINDOW_HEIGHT) / 2.0) / (glutGet(GLUT_WINDOW_HEIGHT) / 2.0);

    printf("Draw Line - x_1_c:%f, y_1_c:%f, x_2_c:%f, y_2_c:%f\n", x_1_c, y_1_c, x_2_c, y_2_c);

    glLineWidth(1.0f);
    glBegin(GL_LINES);
    glVertex3f(x_1_c, y_1_c, 0.0f);
    glVertex3f(x_2_c, y_2_c, 0.0f);
    glEnd();
    glFlush();
}

/*
 * Draws a rectangle from two (x, y) coordinates
 * @param int type - 1 is filled; 2 is outlined
 */
void draw_rectangle(int x_1, int y_1, int x_2, int y_2, int type)
{ 
    float x_1_c = ((float) x_1 - glutGet(GLUT_WINDOW_WIDTH) / 2.0) / (glutGet(GLUT_WINDOW_WIDTH) / 2.0);
    float y_1_c = (-1) * ((float) y_1 - glutGet(GLUT_WINDOW_HEIGHT) / 2.0) / (glutGet(GLUT_WINDOW_HEIGHT) / 2.0);
    float x_2_c = ((float) x_2 - glutGet(GLUT_WINDOW_WIDTH) / 2.0) / (glutGet(GLUT_WINDOW_WIDTH) / 2.0);
    float y_2_c = (-1) * ((float) y_2 - glutGet(GLUT_WINDOW_HEIGHT) / 2.0) / (glutGet(GLUT_WINDOW_HEIGHT) / 2.0);

    printf("Draw Rectangle - x_1_c:%f, y_1_c:%f, x_2_c:%f, y_2_c:%f\n", x_1_c, y_1_c, x_2_c, y_2_c);

    glLineWidth(1.0f);
    if (type == 1)
    {
        glBegin(GL_POLYGON);
    }
    else if (type == 2)
    {
        glBegin(GL_LINE_LOOP);
    }
    glVertex3f(x_1_c, y_1_c, 0.0f);
    glVertex3f(x_1_c, y_2_c, 0.0f);
    glVertex3f(x_2_c, y_2_c, 0.0f);
    glVertex3f(x_2_c, y_1_c, 0.0f);
    glEnd();
    glFlush();
}

/*
 * Draws an ellipse from two (x, y) coordinates. The first being the
 * center coordinates and the second being a point on the outer rim.
 * @param int type - 1 is filled; 2 is outlined
 */
void draw_ellipse(int x_1, int y_1, int x_2, int y_2, int type)
{
    float x_1_c = ((float) x_1 - glutGet(GLUT_WINDOW_WIDTH) / 2.0) / (glutGet(GLUT_WINDOW_WIDTH) / 2.0);
    float y_1_c = (-1) * ((float) y_1 - glutGet(GLUT_WINDOW_HEIGHT) / 2.0) / (glutGet(GLUT_WINDOW_HEIGHT) / 2.0);
    float x_2_c = ((float) x_2 - glutGet(GLUT_WINDOW_WIDTH) / 2.0) / (glutGet(GLUT_WINDOW_WIDTH) / 2.0);
    float y_2_c = (-1) * ((float) y_2 - glutGet(GLUT_WINDOW_HEIGHT) / 2.0) / (glutGet(GLUT_WINDOW_HEIGHT) / 2.0);

    printf("Draw Rectangle - x_1_c:%f, y_1_c:%f, x_2_c:%f, y_2_c:%f\n", x_1_c, y_1_c, x_2_c, y_2_c);

    float radius = sqrt(((x_2_c - x_1_c) * (x_2_c - x_1_c)) + ((y_2_c - y_1_c) * (y_2_c - y_1_c)));
    float theta;
    float ellipse_iterations = 24.0f;

    glLineWidth(2.5f);
    glPolygonMode(GL_FRONT, GL_FILL);
    if (type == 1)
    {
        glPolygonMode(GL_FRONT, GL_FILL);
    }
    else if (type == 2)
    {
        glPolygonMode(GL_FRONT, GL_LINE);
    }
    glBegin(GL_POLYGON);
    glVertex3f(x_1_c + radius, y_1_c, 0.0f);
    for(theta = 0; theta < 2 * M_PI; theta += M_PI / ellipse_iterations)
    {
        glVertex3f(x_1_c + cos(theta) * radius, y_1_c + sin(theta) * radius, 0.0f);
    }
    glEnd();
    glFlush();
}

/*
 * Draws a bezier curve from four (x, y) coordinates. Each coordinate is
 * a control point.
 */
void draw_beziercurve(int x_1, int y_1, int x_2, int y_2, int x_3, int y_3, int x_4, int y_4)
{
    float x_1_c = ((float) x_1 - glutGet(GLUT_WINDOW_WIDTH) / 2.0) / (glutGet(GLUT_WINDOW_WIDTH) / 2.0);
    float y_1_c = (-1) * ((float) y_1 - glutGet(GLUT_WINDOW_HEIGHT) / 2.0) / (glutGet(GLUT_WINDOW_HEIGHT) / 2.0);
    float x_2_c = ((float) x_2 - glutGet(GLUT_WINDOW_WIDTH) / 2.0) / (glutGet(GLUT_WINDOW_WIDTH) / 2.0);
    float y_2_c = (-1) * ((float) y_2 - glutGet(GLUT_WINDOW_HEIGHT) / 2.0) / (glutGet(GLUT_WINDOW_HEIGHT) / 2.0);
    float x_3_c = ((float) x_3 - glutGet(GLUT_WINDOW_WIDTH) / 2.0) / (glutGet(GLUT_WINDOW_WIDTH) / 2.0);
    float y_3_c = (-1) * ((float) y_3 - glutGet(GLUT_WINDOW_HEIGHT) / 2.0) / (glutGet(GLUT_WINDOW_HEIGHT) / 2.0);
    float x_4_c = ((float) x_4 - glutGet(GLUT_WINDOW_WIDTH) / 2.0) / (glutGet(GLUT_WINDOW_WIDTH) / 2.0);
    float y_4_c = (-1) * ((float) y_4 - glutGet(GLUT_WINDOW_HEIGHT) / 2.0) / (glutGet(GLUT_WINDOW_HEIGHT) / 2.0);
 
    printf("Draw Bezier Curve - x_1_c:%f, y_1_c:%f, x_2_c:%f, y_2_c:%f, x_3_c:%f, y_3_c:%f, x_4_c:%f, y_4_c:%f", x_1_c, y_1_c, x_2_c, y_2_c, x_3_c, y_3_c, x_4_c, y_4_c);
    
    GLfloat ctrl_points[4][3] = {
        {x_1_c, y_1_c, 0.0f}, {x_2_c, y_2_c, 0.0f},
        {x_3_c, y_3_c, 0.0f}, {x_4_c, y_4_c, 0.0f}
    };
    
    glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, 4, &ctrl_points[0][0]);
    glEnable(GL_MAP1_VERTEX_3);
    glBegin(GL_LINE_STRIP);
    int i;
    for (i = 0; i <= 50; i++) 
       glEvalCoord1f((GLfloat) i / 50.0f);
    glEnd();
    glFlush();
}

/*
 * Menu options for drawing rectangles of different colors. Rectangles can
 * either be filled or outlined.
 */
void rectangle_menu (int value)
{
  switch (value)
  {
    case 1:
      state_draw_filled_rectangle = 1;
      glColor3f(1.0f, 0.0f, 0.0f);
      printf ("Draw Filled Rectangle -> Red\n");
    break;
    case 2:
      state_draw_filled_rectangle = 1;
      glColor3f(0.0f, 1.0f, 0.0f);
      printf ("Draw Filled Rectangle -> Green\n");
    break;
    case 3:
      state_draw_filled_rectangle = 1;
      glColor3f(0.0f, 0.0f, 1.0f);
      printf ("Draw Filled Rectangle -> Blue\n");
    break;
    case 4:
      state_draw_filled_rectangle = 1;
      glColor3f(1.0f, 1.0f, 0.0f);
      printf ("Draw Filled Rectangle -> Yellow\n");
    break;
    case 5:
      state_draw_filled_rectangle = 1;
      glColor3f(1.0f, 0.0f, 1.0f);
      printf ("Draw Filled Rectangle -> Purple\n");
    break;
    case 6:
      state_draw_filled_rectangle = 1;
      glColor3f(1.0f, 0.5f, 0.0f);
      printf ("Draw Filled Rectangle -> Orange\n");
    break;
    case 7:
      state_draw_filled_rectangle = 1;
      glColor3f(1.0f, 1.0f, 1.0f);
      printf ("Draw Filled Rectangle -> White\n");
    break;
    case 8:
      state_draw_outlined_rectangle = 2;
      glColor3f(1.0f, 0.0f, 0.0f);
      printf ("Draw Outlined Rectangle -> Fill -> Red\n");
    break;
    case 9:
      state_draw_outlined_rectangle = 2;
      glColor3f(0.0f, 1.0f, 0.0f);
      printf ("Draw Outlined Rectangle -> Green\n");
    break;
    case 10:
      state_draw_outlined_rectangle = 2;
      glColor3f(0.0f, 0.0f, 1.0f);
      printf ("Draw Outlined Rectangle -> Blue\n");
    break;
    case 11:
      state_draw_outlined_rectangle = 2;
      glColor3f(1.0f, 1.0f, 0.0f);
      printf ("Draw Outlined Rectangle -> Yellow\n");
    break;
    case 12:
      state_draw_outlined_rectangle = 2;
      glColor3f(1.0f, 0.0f, 1.0f);
      printf ("Draw Outlined Rectangle -> Purple\n");
    break;
    case 13:
      state_draw_outlined_rectangle = 2;
      glColor3f(1.0f, 0.5f, 0.0f);
      printf ("Draw Outlined Rectangle -> Orange\n");
    break;
    case 14:
      state_draw_outlined_rectangle = 2;
      glColor3f(1.0f, 1.0f, 1.0f);
      printf ("Draw Outlined Rectangle -> White\n");
    break;
    default:
    break;
  }
}

/*
 * Options for drawing ellipses of different colors. Ellipses can either be
 * drawn filled or outlined.
 */
void ellipse_menu (int value)
{
  switch (value)
  {
    case 1:
      state_draw_filled_ellipse = 1;
      glColor3f(1.0f, 0.0f, 0.0f);
      printf ("Draw Filled Ellipse -> Fill -> Red\n");
    break;
    case 2:
      state_draw_filled_ellipse = 1;
      glColor3f(0.0f, 1.0f, 0.0f);
      printf ("Draw Filled Ellipse -> Fill -> Green\n");
    break;
    case 3:
      state_draw_filled_ellipse = 1;
      glColor3f(0.0f, 0.0f, 1.0f);
      printf ("Draw Filled Ellipse -> Fill -> Blue\n");
    break;
    case 4:
      state_draw_filled_ellipse = 1;
      glColor3f(1.0f, 1.0f, 0.0f);
      printf ("Draw Filled Ellipse -> Fill -> Yellow\n");
    break;
    case 5:
      state_draw_filled_ellipse = 1;
      glColor3f(1.0f, 0.0f, 1.0f);
      printf ("Draw Filled Ellipse -> Fill -> Purple\n");
    break;
    case 6:
      state_draw_filled_ellipse = 1;
      glColor3f(1.0f, 0.5f, 0.0f);
      printf ("Draw Filled Ellipse -> Fill -> Orange\n");
    break;
    case 7:
      state_draw_filled_ellipse = 1;
      glColor3f(1.0f, 1.0f, 1.0f);
      printf ("Draw Filled Ellipse -> Fill -> White\n");
    break;
    case 8:
      state_draw_outlined_ellipse = 1;
      glColor3f(1.0f, 0.0f, 0.0f);
      printf ("Draw Outlined Ellipse -> Fill -> Red\n");
    break;
    case 9:
      state_draw_outlined_ellipse = 1;
      glColor3f(0.0f, 1.0f, 0.0f);
      printf ("Draw Outlined Ellipse -> Fill -> Green\n");
    break;
    case 10:
      state_draw_outlined_ellipse = 1;
      glColor3f(0.0f, 0.0f, 1.0f);
      printf ("Draw Outlined Ellipse -> Fill -> Blue\n");
    break;
    case 11:
      state_draw_outlined_ellipse = 1;
      glColor3f(1.0f, 1.0f, 0.0f);
      printf ("Draw Outlined Ellipse -> Fill -> Yellow\n");
    break;
    case 12:
      state_draw_outlined_ellipse = 1;
      glColor3f(1.0f, 0.0f, 1.0f);
      printf ("Draw Outlined Ellipse -> Fill -> Purple\n");
    break;
    case 13:
      state_draw_outlined_ellipse = 1;
      glColor3f(1.0f, 0.5f, 0.0f);
      printf ("Draw Outlined Ellipse -> Fill -> Orange\n");
    break;
    case 14:
      state_draw_outlined_ellipse = 1;
      glColor3f(1.0f, 1.0f, 1.0f);
      printf ("Draw Outlined Ellipse -> Fill -> White\n");
    break;
    default:
    break;
  }
}

/**
 * Menu for drawing lines of different colors.
 */
void line_menu (int value)
{
  switch (value)
  {
    case 1:
      state_draw_line = 1;
      glColor3f(1.0f, 0.0f, 0.0f);
      printf ("Draw Line -> Red\n");
    break;
    case 2:
      state_draw_line = 1;
      glColor3f(0.0f, 1.0f, 0.0f);
      printf ("Draw Line -> Green\n");
    break;
    case 3:
      state_draw_line = 1;
      glColor3f(0.0f, 0.0f, 1.0f);
      printf ("Draw Line -> Blue\n");
    break;
    case 4:
      state_draw_line = 1;
      glColor3f(1.0f, 1.0f, 0.0f);
      printf ("Draw Line -> Yellow\n");
    break;
    case 5:
      state_draw_line = 1;
      glColor3f(1.0f, 0.0f, 1.0f);
      printf ("Draw Line -> Purple\n");
    break;
    case 6:
      state_draw_line = 1;
      glColor3f(1.0f, 0.5f, 0.0f);
      printf ("Draw Line -> Orange\n");
    break;
    case 7:
      state_draw_line = 1;
      glColor3f(1.0f, 1.0f, 1.0f);
      printf ("Draw Line -> White\n");
    break;
    default:
    break;
  }
}

/**
 * Menu for drawing bezier curves of different colors.
 */
void bezier_curve_menu (int value)
{
  switch (value)
  {
    case 1:
      state_draw_beziercurve = 1;
      glColor3f(1.0f, 0.0f, 0.0f);
      printf ("Draw Bezier Curve -> Red\n");
    break;
    case 2:
      state_draw_beziercurve = 1;
      glColor3f(0.0f, 1.0f, 0.0f);
      printf ("Draw Bezier Curve -> Green\n");
    break;
    case 3:
      state_draw_beziercurve = 1;
      glColor3f(0.0f, 0.0f, 1.0f);
      printf ("Draw Bezier Curve -> Blue\n");
    break;
    case 4:
      state_draw_beziercurve = 1;
      glColor3f(1.0f, 1.0f, 0.0f);
      printf ("Draw Bezier Curve -> Yellow\n");
    break;
    case 5:
      state_draw_beziercurve = 1;
      glColor3f(1.0f, 0.0f, 1.0f);
      printf ("Draw Bezier Curve -> Purple\n");
    break;
    case 6:
      state_draw_beziercurve = 1;
      glColor3f(1.0f, 0.5f, 0.0f);
      printf ("Draw Bezier Curve -> Orange\n");
    break;
    case 7:
      state_draw_beziercurve = 1;
      glColor3f(1.0f, 1.0f, 1.0f);
      printf ("Draw Bezier Curve -> White\n");
    break;
    default:
    break;
  }
}

/*
 * GLUT callback for glutMouseFunc.
 */
void mouse(int button, int state, int x, int y)
{
    if (state_draw_line && button == GLUT_RIGHT_BUTTON &&
        state == GLUT_DOWN)
    {
        printf("Clicked Line\n");
        num_clicks++;
        if (num_clicks == 1)
        {
            x_1 = x;
            y_1 = y;
        }
        else if (num_clicks == 2)
        {
            x_2 = x;
            y_2 = y;
            draw_line(x_1, y_1, x_2, y_2);
            reset_mouse_clicks();
            state_draw_line = 0;
        }
    }
    else if (state_draw_filled_rectangle && button == GLUT_RIGHT_BUTTON &&
        state == GLUT_DOWN)
    {
        printf("Clicked Rectangle\n");
        num_clicks++;
        if (num_clicks == 1)
        {
            x_1 = x;
            y_1 = y;
        }
        else if (num_clicks == 2)
        {
            x_2 = x;
            y_2 = y;
            draw_rectangle(x_1, y_1, x_2, y_2, 1);
            reset_mouse_clicks();
            state_draw_filled_rectangle = 0;
        }
    }
    else if (state_draw_outlined_rectangle && button == GLUT_RIGHT_BUTTON &&
        state == GLUT_DOWN)
    {
        printf("Clicked Rectangle\n");
        num_clicks++;
        if (num_clicks == 1)
        {
            x_1 = x;
            y_1 = y;
        }
        else if (num_clicks == 2)
        {
            x_2 = x;
            y_2 = y;
            draw_rectangle(x_1, y_1, x_2, y_2, 2);
            reset_mouse_clicks();
            state_draw_outlined_rectangle = 0;
        }
    }
    else if (state_draw_filled_ellipse && button == GLUT_RIGHT_BUTTON &&
        state == GLUT_DOWN)
    {
        printf("Clicked Ellipse\n");
        num_clicks++;
        if (num_clicks == 1)
        {
            x_1 = x;
            y_1 = y;
        }
        else if (num_clicks == 2)
        {
            x_2 = x;
            y_2 = y;
            draw_ellipse(x_1, y_1, x_2, y_2, 1);
            reset_mouse_clicks();
            state_draw_filled_ellipse = 0;
        }
    }
    else if (state_draw_outlined_ellipse && button == GLUT_RIGHT_BUTTON &&
        state == GLUT_DOWN)
    {
        printf("Clicked Ellipse\n");
        num_clicks++;
        if (num_clicks == 1)
        {
            x_1 = x;
            y_1 = y;
        }
        else if (num_clicks == 2)
        {
            x_2 = x;
            y_2 = y;
            draw_ellipse(x_1, y_1, x_2, y_2, 2);
            reset_mouse_clicks();
            state_draw_outlined_ellipse = 0;
        }
    }
    else if (state_draw_beziercurve && button == GLUT_RIGHT_BUTTON &&
        state == GLUT_DOWN)
    {
        printf("Clicked Bezier Curve\n");
        num_clicks++;
        if (num_clicks == 1)
        {
            x_1 = x;
            y_1 = y;
        }
        else if (num_clicks == 2)
        {
            x_2 = x;
            y_2 = y;
        }
        else if (num_clicks == 3)
        {
            x_3 = x;
            y_3 = y;
        }
        else if (num_clicks == 4)
        {
            x_4 = x;
            y_4 = y;
            draw_beziercurve(x_1, y_1, x_2, y_2, x_3, y_3, x_4, y_4);
            reset_mouse_clicks();
            state_draw_beziercurve = 0;
        }
    }
}

/*
 * Initializes the menu. Options for drawing rectangles, ellipses, lines, and
 * bezier curves.
 */
void init_menu()
{
    int rectangle_type_menu, rectangle_fill_menu, rectangle_outline_menu,
    ellipse_type_menu, ellipse_fill_menu, ellipse_outline_menu,
    line_color_menu,
    bezier_curve_color_menu;

  rectangle_fill_menu = glutCreateMenu (rectangle_menu);
  glutAddMenuEntry ("Red", 1);
  glutAddMenuEntry ("Green", 2);
  glutAddMenuEntry ("Blue", 3);
  glutAddMenuEntry ("Yellow", 4);
  glutAddMenuEntry ("Purple", 5);
  glutAddMenuEntry ("Orange", 6);
  glutAddMenuEntry ("White", 7);
  rectangle_outline_menu = glutCreateMenu (rectangle_menu);
  glutAddMenuEntry ("Red", 8);
  glutAddMenuEntry ("Green", 9);
  glutAddMenuEntry ("Blue", 10);
  glutAddMenuEntry ("Yellow", 11);
  glutAddMenuEntry ("Purple", 12);
  glutAddMenuEntry ("Orange", 13);
  glutAddMenuEntry ("White", 14);
  rectangle_type_menu = glutCreateMenu (rectangle_menu);
  glutAddSubMenu ("Fill", rectangle_fill_menu);
  glutAddSubMenu ("Outline", rectangle_outline_menu);
  glutCreateMenu (rectangle_menu);

  ellipse_fill_menu = glutCreateMenu (ellipse_menu);
  glutAddMenuEntry ("Red", 1);
  glutAddMenuEntry ("Green", 2);
  glutAddMenuEntry ("Blue", 3);
  glutAddMenuEntry ("Yellow", 4);
  glutAddMenuEntry ("Purple", 5);
  glutAddMenuEntry ("Orange", 6);
  glutAddMenuEntry ("White", 7);
  ellipse_outline_menu = glutCreateMenu (ellipse_menu);
  glutAddMenuEntry ("Red", 8);
  glutAddMenuEntry ("Green", 9);
  glutAddMenuEntry ("Blue", 10);
  glutAddMenuEntry ("Yellow", 11);
  glutAddMenuEntry ("Purple", 12);
  glutAddMenuEntry ("Orange", 13);
  glutAddMenuEntry ("White", 14);
  ellipse_type_menu = glutCreateMenu (ellipse_menu);
  glutAddSubMenu ("Fill", ellipse_fill_menu);
  glutAddSubMenu ("Outline", ellipse_outline_menu);
  glutCreateMenu (ellipse_menu);

  line_color_menu = glutCreateMenu (line_menu);
  glutAddMenuEntry ("Red", 1);
  glutAddMenuEntry ("Green", 2);
  glutAddMenuEntry ("Blue", 3);
  glutAddMenuEntry ("Yellow", 4);
  glutAddMenuEntry ("Purple", 5);
  glutAddMenuEntry ("Orange", 6);
  glutAddMenuEntry ("White", 7);
  glutCreateMenu (line_menu);

  bezier_curve_color_menu = glutCreateMenu (bezier_curve_menu);
  glutAddMenuEntry ("Red", 1);
  glutAddMenuEntry ("Green", 2);
  glutAddMenuEntry ("Blue", 3);
  glutAddMenuEntry ("Yellow", 4);
  glutAddMenuEntry ("Purple", 5);
  glutAddMenuEntry ("Orange", 6);
  glutAddMenuEntry ("White", 7);
  glutCreateMenu (bezier_curve_menu);

  glutAddSubMenu ("Draw Rectangle", rectangle_type_menu);;
  glutAddSubMenu ("Draw Ellipse", ellipse_type_menu);
  glutAddSubMenu ("Draw Line", line_color_menu);
  glutAddSubMenu ("Draw Bezier Curve", bezier_curve_color_menu);
}

int main (int argc, char *argv[])
{
    const char *WINDOW_TITLE = "Paint and Draw";

    glutInit(&argc, argv);
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutCreateWindow(WINDOW_TITLE);

    init_display();
    init_menu();
    glutAttachMenu(GLUT_LEFT_BUTTON);
    glutDisplayFunc(display);
    glutMouseFunc(mouse);
    glutMainLoop();
}
